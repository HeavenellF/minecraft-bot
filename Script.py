import time
import pyautogui
import keyboard

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

afkBot_lastcall = 0
afkBot_Cooldown_duration = 10
abayoBot_lastcall = 0
abayo_Cooldown_duration = 30

def chatby_System(last_line,secondlast_line):
    current_time = time.time()
    #print('chatby_System')
    if ' joined the game' in last_line :
        Name_Splitter = last_line.split(' ')
        Name_Joined = Name_Splitter[5]
        #print('<' + Name_Joined + '>')
        with open(r'D:\Code\Minecarft Bot\greeting.txt','r') as friendlist:
        #with open(r'D:\UNI\greeting.txt','r') as friendlist:
            for list in friendlist:
                if Name_Joined in list :
                    greeting = list.split(' ')[1].strip()
                    break
                else :
                    greeting = 'hi'
        with open(r'D:\Code\Minecarft Bot\SavesCooldown.txt', 'r+') as cooldownlist:
            lines = cooldownlist.readlines()
            found = False
            for i, line in enumerate(lines):
                if Name_Joined in line:
                    last_joined = float(line.split(' ')[1].strip())
                    if current_time - last_joined > 10:
                        pyautogui.press('enter')
                        pyautogui.typewrite(greeting)
                        pyautogui.press('enter')
                        lines[i] = f'{Name_Joined} {current_time}\n'
                        cooldownlist.seek(0)
                        cooldownlist.writelines(lines)
                        cooldownlist.truncate()
                    found = True
                    break
            if not found:
                pyautogui.press('enter')
                pyautogui.typewrite(greeting)
                pyautogui.press('enter')
                cooldownlist.write(f'{Name_Joined} {current_time}\n')
    elif ' has sent a request to teleport to you.' in secondlast_line:
        pyautogui.press('enter')
        pyautogui.typewrite('/tpaccept')
        pyautogui.press('enter')

def chatby_Player(last_line):
    global afkBot_lastcall
    global afkBot_Cooldown_duration
    global abayoBot_lastcall
    global abayo_Cooldown_duration
    MyNameList = [' heaven', ' heavenell']
    if any(name in last_line.lower() for name in MyNameList):
        current_time = time.time()
        if current_time - afkBot_lastcall > afkBot_Cooldown_duration:
            afkBot_lastcall = current_time
            if '<Heavenell>' not in last_line :
                if ' whispers to you:' not in last_line :
                    pyautogui.press('enter')
                    pyautogui.typewrite(',onemoment')
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('Hmph...')
                    pyautogui.press('enter')
    if ' whispers to you:' in last_line:
        if last_line.split(' ')[6].split(' ')[0] == '' :
            if len(last_line.split('^')) == 3:
                #if len(last_line.split('/')) == 2:
                copy = last_line.split('^')[1].split('^')[0]
                #whisperer = last_line.split(' ')[6].split(' ')[0]
                print('copied')
                pyautogui.press('enter')
                #pyautogui.typewrite(whisperer)
                #pyautogui.typewrite(' : ')
                pyautogui.typewrite(copy)
                pyautogui.press('enter')

    goodbyeMessage = ['abayo','matanene','goodbye','goodnight']
    if any(name in last_line.lower() for name in goodbyeMessage):
        current_time = time.time()
        if current_time - abayoBot_lastcall > abayo_Cooldown_duration:
            abayoBot_lastcall = current_time
            pyautogui.press('enter')
            time.sleep(0.2)
            pyautogui.typewrite('matanene')
            pyautogui.press('enter')

def keyboardOff():
    keyboard.release('shift')
    keyboard.block_key('a')
    keyboard.block_key('s')
    keyboard.block_key('w')
    keyboard.block_key('d')
    keyboard.block_key('shift')
    keyboard.block_key('space')

def keyboardOn():
    keyboard.unblock_key('a')
    keyboard.unblock_key('s')
    keyboard.unblock_key('w')
    keyboard.unblock_key('d')
    keyboard.unblock_key('shift')
    keyboard.unblock_key('space')

class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if not event.is_directory and event.src_path.endswith("latest.log"):
            #print("File modified")
            with open(path2, 'r') as file:
                lines = file.readlines()
                last_line = lines[-1]
                secondlast_line = lines[-2]
                delimiter = last_line.split('[')
                secureChat = delimiter[3] if len(delimiter) >= 4 else None
                keyboardOff()
                if secureChat == 'System] ' :
                    chatby_System(last_line,secondlast_line) 
                elif secureChat == 'Not Secure] ':
                    chatby_Player(last_line)
                keyboardOn()
                    

if __name__ == "__main__":
    #Path for File Triggering
    path2 = r'C:\Users\user\AppData\Roaming\.minecraft\logs\latest.log'
    #Path for File Monitoring
    path = r"C:\Users\user\AppData\Roaming\.minecraft\logs"
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=False)
    observer.start()

    print("Watching for changes to", path)

    try:
        while True:
            time.sleep(0.01)
            open(path2, 'r') # to trigger the Changes
    except KeyboardInterrupt:
        observer.stop()

    observer.join()